package com.nokia.cbam.example;

import java.util.List;

public class StringUtils {

    public static String joiner(List<String> numbersToShowJoin, String delimiter) {
        String join = "";

        for (int i = 0; i < numbersToShowJoin.size()-1; i++) {
            String actual = numbersToShowJoin.get(i);
            if (actual != null) {
                join += actual +delimiter;
            }
        }
        join += numbersToShowJoin.get(numbersToShowJoin.size()-1);
        return join;
    }
}
