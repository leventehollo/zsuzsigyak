package com.nokia.cbam.example;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StringUtilsTest {

    List<String> inputList;


    @Before
    public void prepare() {
        this.inputList = Lists.newArrayList("a", "b", "c", "d", "e");
    }

    @Test
    public void joinerWorks() throws Exception {
        String delimiter = ";";

        String joiner = StringUtils.joiner(inputList, delimiter);
        Assert.assertEquals(Joiner.on(delimiter).join(inputList), joiner);
    }

    @Test
    public void joinerWithNull() throws Exception {
        inputList.set(2, null);
        String delimiter = ",";

        String joiner = StringUtils.joiner(inputList, delimiter);
        Assert.assertEquals("asd", joiner);
    }


}